//
// GQLJson object
//

#include "../GQLEngine.h"

using namespace std;

namespace TangoGQL_ns {

GQLJson::GQLJson() {
  request = nullptr;
}
GQLJson::~GQLJson() {
  delete request;
}

// Parse a GQL request as JSON
void GQLJson::parse(GQLSchema *schema,string &jsonStr) {

  // JSON
  // A GraphQL JSON request can have 3 keys:
  //{
  //  "query": "...",
  //  "operationName": "...",
  //  "variables": { "myVariable": "someValue", ... }
  //}

  bool eof = false;
  string jsonQuery;
  Parser jsonParser(jsonStr,"JSON");
  variables.clear();
  jsonParser.jumpSep('{');
  while(!eof) {

    string key;
    jsonParser.readWord(key);

    if( key=="query" ) {
      jsonParser.jumpSep(':');
      jsonParser.readWord(jsonQuery);
    } else if( key=="operationName" ) {
      jsonParser.jumpSep(':');
      jsonParser.readWord(operationName);
    } else if( key=="variables" ) {
      jsonParser.jumpSep(':');
      try {
        jsonParser.parseJSON(variables);
      } catch (string& err) {
        throw err + ":variables";
      }
    } else {
      throw string("Unexpected JSON field: " + key);
    }

    jsonParser.jumpSpace();
    if (jsonParser.current()=='}') {
      jsonParser.jumpSep('}');
      eof = true;
    } else if(jsonParser.current()==0) {
      throw string("Unexpected end of file '}' missing, JSON field:" + key);
    } else {
      jsonParser.jumpSep(',');
    }

  }

  if(jsonQuery.length()==0)
    throw string("Query not found in JSON");

  // GQL query and fragments
  Parser queryParser(jsonQuery,"JSON:query");

  std::vector<GQLRequest *> fragments;
  eof = false;
  while(!eof) {
    GQLRequest *req = new GQLRequest();
    try {
      req->parse(schema, &queryParser, &variables);
    } catch (string& err) {
      delete req;
      for(auto & fragment : fragments) delete fragment;
      throw string(err);
    }

    if(req->type=="query" ||
       req->type=="mutation" ||
       req->type=="subscription" ) {
      request = req;
    } else if(req->type=="fragment") {
      fragments.push_back(req);
    } else {
      for(auto & fragment : fragments) delete fragment;
      throw string("Unexpected root query type '" + req->type + "'");
    }
    eof = queryParser.eof();
  }

  if(request == nullptr) {
    for (auto &fragment : fragments) delete fragment;
    throw string("Incomplete request. No query,mutation or subscription found");
  }

  // Expand fragments
  try {
    request->expandFragments(&queryParser, fragments);
  } catch (string& err) {
    for(auto & fragment : fragments) delete fragment;
    throw string(err);
  }

  for(auto & fragment : fragments)
    delete fragment;

}

void GQLJson::execute(void *rootNode,std::string &result) const {

  // Check variables
  GQLInputArgs& args = request->inputArgs;
  for(auto v : args.args) {
    if(v->name.empty())
      throw string("Invalid variable name in "+request->name);
    string vName = v->name.substr(1);
    if(vName.empty() || v->name[0]!='$')
      throw string("Invalid variable name '"+v->name+"' in "+request->name+", must start with '$'");
    if(variables.find(vName)==variables.end())
      throw string("Variable name '"+vName+"' not found in JSON:variables");
  }

  request->execute(rootNode,result);
}

vector<GQLSubscription *> GQLJson::executeSubscription(void *rootNode) const {
  return request->executeSubscription(rootNode);
}

} // namespace TangoGQL_ns
