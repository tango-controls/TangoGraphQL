//
// GQLUtils utility class
//

#include "GQLUtils.h"
#include <inttypes.h>
#include <string.h>
#include "fpconv.h"

namespace TangoGQL_ns {

const char* digits[] = {
        "00","10","20","30","40","50","60","70","80","90",
        "01","11","21","31","41","51","61","71","81","91",
        "02","12","22","32","42","52","62","72","82","92",
        "03","13","23","33","43","53","63","73","83","93",
        "04","14","24","34","44","54","64","74","84","94",
        "05","15","25","35","45","55","65","75","85","95",
        "06","16","26","36","46","56","66","76","86","96",
        "07","17","27","37","47","57","67","77","87","97",
        "08","18","28","38","48","58","68","78","88","98",
        "09","19","29","39","49","59","69","79","89","99"
};

void GQLUtils::quote(const std::string& value,std::string& evalStr,bool doEscape) {
  evalStr.push_back('"');
  if(doEscape)
    escape(value,evalStr);
  else
    evalStr.append(value);
  evalStr.push_back('"');
}

void GQLUtils::quoteOrNull(const std::string& value,std::string& evalStr,bool doEscape) {
  if(value.empty()) {
    evalStr.append("null");
  } else {
    quote(value,evalStr,doEscape);
  }
}

void GQLUtils::noQuoteOrNull(const std::string& value,std::string& evalStr,bool doEscape) {
  if(value.empty()) {
    evalStr.append("null");
  } else {
    if(doEscape)
      escape(value,evalStr);
    else
      evalStr.append(value);
  }
}
void GQLUtils::unescape(const std::string &in,std::string& evalStr) {

  size_t i = 0;
  size_t l = in.size();
  while(i<l-1) {
    if(in[i]=='\\' && in[i+1]=='n') {
      evalStr.push_back('\n');
      i += 2;
    } else if(in[i]=='\\' && in[i+1]=='t') {
      evalStr.push_back('\t');
      i += 2;
    } else if(in[i]=='\\' && in[i+1]=='\\') {
      evalStr.push_back('\\');
      i += 2;
    } else if(in[i]=='\\' && in[i+1]=='\"') {
      evalStr.push_back('"');
      i += 2;
    } else {
      evalStr.push_back(in[i]);
      i++;
    }
  }
  if(i<l)
    evalStr.push_back(in[i]);

}

void GQLUtils::escape(const std::string& in,std::string& evalStr) {
  for(char c : in) {
    if(c<32) {
      switch(c) {
        case '\t':
          evalStr.push_back('\\');
          evalStr.push_back('t');
          break;
        case '\n':
          evalStr.push_back('\\');
          evalStr.push_back('n');
          break;
        case '\r':
          evalStr.push_back('\\');
          evalStr.push_back('r');
          break;
        default:
          // Unhandled escape sequence
          // Ignore char
          break;
      }
    } else {
      switch(c) {
        case '\\':
          evalStr.push_back('\\');
          evalStr.push_back('\\');
          break;
        case '"':
          evalStr.push_back('\\');
          evalStr.push_back('"');
          break;
        default:
          evalStr.push_back(c);
      }
    }
  }
}

void GQLUtils::stringArr(const std::vector<std::string>& strArr,std::string& evalStr,bool doEscape) {

  evalStr.push_back('[');
  for(size_t i=0;i<strArr.size();i++) {
    GQLUtils::quote(strArr[i], evalStr, doEscape);
    if(i<strArr.size()-1) evalStr.push_back(',');
  }
  evalStr.push_back(']');


}

void GQLUtils::String(const std::string& in,std::string& evalStr) {
  evalStr.push_back('\"');
  escape(in,evalStr);
  evalStr.push_back('\"');
}

void GQLUtils::Bool(const bool value,std::string& evalStr) {
   if(value)
     evalStr.append("true");
   else
     evalStr.append("false");
}

void GQLUtils::Int64(const int64_t value,std::string& evalStr) {

  if(value<0) {
    evalStr.push_back('-');
    UInt64((uint64_t)-value,evalStr);
  } else {
    UInt64((uint64_t)value,evalStr);
  }

}

void GQLUtils::UInt64(const uint64_t value,std::string& evalStr) {


#if 1

  char tmp[32];
  uint64_t v = value;
  uint64_t digit;
  int pos = 0;
  while(v>=100) {
    digit = v % 100;
    v = v / 100;
    tmp[pos++] = digits[digit][0];
    tmp[pos++] = digits[digit][1];
  }
  if(v>=10) {
    digit = v % 10;
    v = v / 10;
    tmp[pos++] = (char)('0' + digit);
  }
  tmp[pos++] = (char)('0' + v);

  for(int i=pos-1;i>=0;i--)
    evalStr.push_back(tmp[i]);

#else

  evalStr.append(std::to_string(value));

#endif

}

void GQLUtils::Double(const double value,std::string& evalStr) {

  char bStr[32];

#ifdef FPCONV
  int str_len = fpconv_dtoa(value, bStr);
  evalStr.append(bStr,str_len);
#else
  ::sprintf(bStr,"%g",v64);
  evalStr.append(bStr);
#endif

}

int64_t GQLUtils::parseInt(const std::string& valueStr) {

  if(valueStr[0]=='-') {
    return (int64_t)-parseUInt(valueStr.substr(1));
  } else {
    return (int64_t)parseUInt(valueStr);
  }

}

uint64_t GQLUtils::parseUInt(const std::string& valueStr) {

  uint64_t ret;
  if(valueStr.empty())
    throw std::string("Cannot convert empty string to integer");
  char* end;

  if(valueStr.substr(0,2)=="0x")
    ret = strtoull(valueStr.c_str(), &end, 16);
  else
    ret = strtoull(valueStr.c_str(), &end, 10);

  if (end==valueStr.c_str())
    throw std::string("Cannot convert '"+valueStr+"' to integer");

  return ret;

}

double GQLUtils::parseDouble(const std::string& valueStr) {

  if(valueStr.empty())
    throw std::string("Cannot convert empty string to double");
  char* end;
  double ret = strtod(valueStr.c_str(), &end);
  if (end==valueStr.c_str())
    throw std::string("Cannot convert '"+valueStr+"' to double");

  return ret;

}

bool GQLUtils::parseBoolean(const std::string& valueStr) {

  if(valueStr.empty())
    throw std::string("Cannot convert empty string to boolean");
  if(::strcasecmp(valueStr.c_str(),"true")==0)
    return true;
  if(::strcasecmp(valueStr.c_str(),"false")==0)
    return false;
  throw std::string("Cannot convert '"+valueStr+"' to boolean");

}

bool GQLUtils::match(const char *str, const char *pattern, bool caseSensitive) {

  const char *s;
  const char *p;
  bool star = false;

  loopStart:
  for (s = str, p = pattern; *s; ++s, ++p) {

    switch (*p) {
      case '?':
        if (*s == '.') goto starCheck;
        break;

      case '*':
        star = true;
        str = s, pattern = p;
        if (!*++pattern) return true;
        goto loopStart;

      default:
        if (caseSensitive) {
          if (*s != *p)
            goto starCheck;
        } else {
          if (tolower(*s) != tolower(*p))
            goto starCheck;
        }
        break;
    } /* endswitch */

  } /* endfor */

  if (*p == '*') ++p;
  return (!*p);

  starCheck:
  if (!star) return false;
  str++;
  goto loopStart;

}

} // namespace TangoGQL_ns