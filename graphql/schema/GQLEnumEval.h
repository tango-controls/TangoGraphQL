//
// GQL enum eval functions
//

#ifndef TANGOGQL_GQLENUMEVAL_H
#define TANGOGQL_GQLENUMEVAL_H
#include "GQLEnum.h"
#include "GQLEval.h"
#include "../utils/GQLUtils.h"

namespace TangoGQL_ns {

class GQLEnum;

BEGIN_EVAL(GQLEvalEnumName)
    GQLEnum *e = (GQLEnum *)node;
    GQLUtils::quote(e->name,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalEnumDescription)
    GQLEnum *e = (GQLEnum *)node;
    GQLUtils::quoteOrNull(e->description,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalEnumIsDeprecated)
    GQLEnum *e = (GQLEnum *)node;
    if(e->isDeprecated)
      evalStr.append("true");
    else
      evalStr.append("false");
END_EVAL

BEGIN_EVAL(GQLEvalEnumDepreciationReason)
    GQLEnum *e = (GQLEnum *)node;
    GQLUtils::quoteOrNull(e->deprecationReason,evalStr);
END_EVAL

} // namespace TangoGQL_ns

#endif //TANGOGQL_GQLENUMEVAL_H
