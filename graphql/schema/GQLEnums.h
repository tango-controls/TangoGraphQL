//
// GQLEnums object
//

#ifndef TANGOGQL_GQLENUMS_H
#define TANGOGQL_GQLENUMS_H
#include "GQLEnum.h"

namespace TangoGQL_ns {

class GQLEngine;

class GQLEnums {

public:
  GQLEnums();
  ~GQLEnums();

  std::vector<GQLEnum *> enums;
  std::string to_string() const;
  void parse(Parser* parser);

};

} // namespace TangoGQL_ns

#endif //TANGOGQL_GQLENUMS_H
