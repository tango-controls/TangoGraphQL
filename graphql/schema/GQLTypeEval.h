//
// GQL type eval functions
//

#ifndef TANGOGQL_GQLTYPEEVAL_H
#define TANGOGQL_GQLTYPEEVAL_H

#include "GQLType.h"
#include "../utils/GQLUtils.h"

namespace TangoGQL_ns {

class GQLType;

BEGIN_EVAL(GQLEvalTypeName)
    GQLType *type = (GQLType *)node;
    if(type->ofType==nullptr) {
      GQLUtils::quote(type->name, evalStr);
    } else {
      GQLUtils::quoteOrNull(type->name, evalStr);
    }
END_EVAL

BEGIN_EVAL(GQLEvalTypeKind)
    GQLType *type = (GQLType *)node;
    GQLUtils::quote(GQLType::GetKind(type->kind),evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalTypeDescription)
    GQLType *type = (GQLType *)node;
    GQLUtils::quote(type->description,evalStr);
END_EVAL

BEGIN_EVAL(GQLEvalTypeFields)
    GQLType *type = (GQLType *)node;
    PTRARRAYORNULL(type->visibleFields);
END_EVAL

BEGIN_EVAL(GQLEvalTypeInterfaces)
    evalStr.append("[]");
END_EVAL

BEGIN_EVAL(GQLEvalTypeEnumValues)
    GQLType *type = (GQLType *)node;
    GQLEnums *e = &type->enumValues;
    PTRARRAYORNULL(e->enums);
END_EVAL

BEGIN_EVAL(GQLEvalTypeInputFields)
    evalStr.append("null");
END_EVAL

BEGIN_EVAL(GQLEvalTypeOfType)
    GQLType *type = (GQLType *)node;
    children->execute((void *)  type->ofType, evalStr);
END_EVAL

// Meta queries
// __type(name:String)
DECLARE_EVAL(GQLEvalQuery__type)
END_DECLARE_EVAL


} // namespace TangoGQL_ns

#endif //TANGOGQL_GQLTYPEEVAL_H
