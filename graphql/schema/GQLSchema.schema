R"(
# Do not remove the first line
# GraphQL schema introspection system (http://spec.graphql.org/draft/#sec-Type-System)

# A signed 32 bit integer.
scalar Int
# A signed double precision floating point value.
scalar Float
# A UTF 8 character sequence.
scalar String
# true or false.
scalar Boolean
#  The ID scalar type represents a unique identifier.
scalar ID
# The ScalarTypes represents a generic scalar value that could be: Int, String, Boolean, Float, List.
scalar ScalarTypes

enum __TypeKind {
  # Indicates this type is a scalar.
  SCALAR
  # Indicates this type is an object. fields and interfaces are valid fields.
  OBJECT
  # Indicates this type is an interface. fields and possibleTypes are valid fields.
  INTERFACE
  # Indicates this type is a union. possibleTypes is a valid field.
  UNION
  # Indicates this type is an enum. enumValues is a valid field.
  ENUM
  # Indicates this type is an input object. inputFields is a valid field.
  INPUT_OBJECT
  # Indicates this type is a list. ofType is a valid field.
  LIST
  # Indicates this type is a non-null. ofType is a valid field.
  NON_NULL
}

# The schema introspection system is accessible from the metafields __schema and __type which
# are accessible from the type of the root of a query operation.
# __schema: __Schema!
# __type(name: String!): __Type
type __Schema {
  description: String
  # A list of all types supported by this server.
  types: [__Type!]!
  # The type that query operations will be rooted at.
  queryType: __Type!
  # If this server supports mutation, the type that mutation operations will be rooted at.
  mutationType: __Type
  # If this server support subscription, the type that subscription operations will be rooted at.
  subscriptionType: __Type
  # A list of all directives supported by this server.
  directives: [__Directive!]!
}

# The fundamental unit of any GraphQL Schema is the type. There are many kinds of types in GraphQL
# as represented by the __TypeKind enum.
# Depending on the kind of a type, certain fields describe information about that type. Scalar types
# provide no information beyond a name and description, while Enum types provide their values. Object
# and Interface types provide the fields they describe. Abstract types, Union and Interface, provide
# the Object types possible at runtime. List and NonNull types compose other types.
type __Type {
  kind: __TypeKind!
  name: String
  description: String

  # should be non-null for OBJECT and INTERFACE only, must be null for the others
  fields(includeDeprecated: Boolean = false): [__Field!]

  # should be non-null for OBJECT and INTERFACE only, must be null for the others
  interfaces: [__Type!]

  # should be non-null for INTERFACE and UNION only, always null for the others
  possibleTypes: [__Type!]

  # should be non-null for ENUM only, must be null for the others
  enumValues(includeDeprecated: Boolean = false): [__EnumValue!]

  # should be non-null for INPUT_OBJECT only, must be null for the others
  inputFields: [__InputValue!]

  # should be non-null for NON_NULL and LIST only, must be null for the others
  ofType: __Type
}

# Object and Interface types are described by a list of Fields, each of which has a name, potentially
# a list of arguments, and a return type.
type __Field {
  name: String!
  description: String
  args: [__InputValue!]!
  type: __Type!
  isDeprecated: Boolean!
  deprecationReason: String
}

# Arguments provided to Fields or Directives and the input fields of an InputObject are represented as
# Input Values which describe their type and optionally a default value.
type __InputValue {
  name: String!
  description: String
  type: __Type!
  defaultValue: String
}

# One possible value for a given Enum. Enum values are unique values, not a placeholder for a string or
# numeric value. However an Enum value is returned in a JSON response as a string.
type __EnumValue {
  name: String!
  description: String
  isDeprecated: Boolean!
  deprecationReason: String
}


# A Directive provides a way to describe alternate runtime execution and type validation behavior in a
# GraphQL document. In some cases, you need to provide options to alter GraphQL's execution behavior
# in ways field arguments will not suffice, such as conditionally including or skipping a field. Directives
# provide this by describing additional information to the executor.
type __Directive {
  name: String!
  description: String
  locations: [__DirectiveLocation!]!
  args: [__InputValue!]!
  isRepeatable: Boolean!
}

enum __DirectiveLocation {
  QUERY
  MUTATION
  SUBSCRIPTION
  FIELD
  FRAGMENT_DEFINITION
  FRAGMENT_SPREAD
  INLINE_FRAGMENT
  VARIABLE_DEFINITION
  SCHEMA
  SCALAR
  OBJECT
  FIELD_DEFINITION
  ARGUMENT_DEFINITION
  INTERFACE
  UNION
  ENUM
  ENUM_VALUE
  INPUT_OBJECT
  INPUT_FIELD_DEFINITION
}


#-------------------------------------------------------------------------------
# Tango GraphQL schema
#-------------------------------------------------------------------------------

# Database device browsing.
# Domain item.
type DbDomain {
  # Domain name
  name:String!
  # Families, return null in case of failure.
  families(wildcard:String = "*"):[DbFamily!]
  # Error string or null when data are available or when mutation was ok. By default it returns only the
  # description field of the top level error stack. The error field must be placed at the end of the block.
  error(getStack:Boolean = false): [String]
}

# Database device browsing.
# Family item.
type DbFamily {
  # Family name
  name:String!
  # Members, return null in case of failure.
  members(wildcard:String = "*"):[DbMember!]
  # Error string or null when data are available or when mutation was ok. By default it returns only the
  # description field of the top level error stack. The error field must be placed at the end of the block.
  error(getStack:Boolean = false): [String]
}

# Database device browsing.
# Member item.
type DbMember {
  # Member name
  name:String!
}

# Database server browsing.
# Server item.
type DbServer {
  # Server name
  name:String!
  # Instance list, return null in case of failure.
  instances(wildcard:String = "*"):[DbInstance!]
  # Error string or null when data are available or when mutation was ok. By default it returns only the
  # description field of the top level error stack. The error field must be placed at the end of the block.
  error(getStack:Boolean = false): [String]
}

# Database server browsing.
# Instance item.
type DbInstance {
  # Instance name
  name:String!
  # class list, return null in case of failure.
  classes(wildcard:String = "*"):[DbClass!]
  # Error string or null when data are available or when mutation was ok. By default it returns only the
  # description field of the top level error stack. The error field must be placed at the end of the block.
  error(getStack:Boolean = false): [String]
}

# Database server browsing.
# Class item.
type DbClass {
  # Class name
  name:String!
  # device list, return null in case of failure.
  devices(wildcard:String = "*"):[DbDevice!]
  # Error string or null when data are available or when mutation was ok. By default it returns only the
  # description field of the top level error stack. The error field must be placed at the end of the block.
  error(getStack:Boolean = false): [String]
}

# Database server browsing.
# Device item.
type DbDevice {
  # Device name
  name:String!
}

# Property item
type DbDatum {
  # Property name
  name:String!
  # Attribute name for device attribute properties, null otherwise.
  att_name:String
  # Property value, [] if property is empty
  value:[String!]!
}

# Device information
type DbDeviceInfo {
  # Device name
  dev_name:String!
  # The device class name
  class_name:String!
  # The full device server process name
  ds_full_name:String!
  # The host name where the device server process is running
  host:String!
  # Date of the last device export (empty if not set in DB)
  started_date:String!
  # Date of the last device un-export (empty if not set in DB)
  stopped_date:String!
  # The device server process PID (-1 if not set in DB)
  pid:Int!
}

# Type of database query
enum DbQueryType {
  # Query list of hosts
  HOST
  # Query list of servers
  SERVER
  # Query devices (list, properties)
  DEVICE
  # Query classes (list, properties)
  CLASS
  # Query object (list, properties)
  OBJECT
  # Query list of exported devices
  EXPORTED_DEVICE
}

# Database query objet
type DatabaseRead {

  # Database hostname
  host_name: String

  # Database port
  port: Int

  # Database info
  info:String

  # Get HOST,SERVER,DEVICE,CLASS or OBJECT list, return null in case of failure.
  list(type:DbQueryType!,wildcard:String = "*"):[String!]

  # Get DEVICE,CLASS or OBJECT properties list, return null in case of failure.
  property_list(type:DbQueryType!,name:String!,wildcard:String = "*"):[String!]

  # Get DEVICE,CLASS or OBJECT properties, return null in case of failure.
  # You can use a single wildcard string for propNames (eg propNames:[''*'']).
  properties(type:DbQueryType!,name:String!,propNames:[String!]!):[DbDatum!]

  # Get all DEVICE or CLASS attribute property, return null in case of failure.
  attribute_properties(type:DbQueryType!,name:String!,attName:String!):[DbDatum!]

  # Database browsing function
  # Get domains, return null in case of failure.
  domains(wildcard:String = "*"):[DbDomain!]

  # Database browsing function
  # Get servers, return null in case of failure.
  servers(wildcard:String = "*"):[DbServer!]

  # Get device info from the DB, return null in case of failure.
  device_info(name:String!):DbDeviceInfo

  # Error string or null when data are available or when mutation was ok. By default it returns only the
  # description field of the top level error stack. The error field must be placed at the end of the block.
  error(getStack:Boolean = false): [String]

}

# Device query object
type DeviceRead {

  # Device name
  name: String!

  # Read attributes. Returns null on major failure. If an attribute reading fails, the error field of the returned DeviceAttribute is set.
  read_attributes(names:[String!]!) : [DeviceAttribute!]

  # Read attribute polling history. Returns null on major failure. If the reading fails, the error field of the returned DeviceAttribute is set.
  read_attribute_hist(name:String!,depth:Int!) : [DeviceAttribute!]

  # Get attribute list. Returns null when fails.
  attribute_list: [String!]
  # Read attribute properties. Returns null when fails. If one or more attribute in the given list does not exist, the whole call fails.
  # You can use names:[''*''] to get properties for the whole set of attributes.
  attribute_info(names:[String!] = ["*"]) : [AttributeInfo!]

  # Get command list. Returns null when fails.
  command_list: [String!]
  # Read command properties. Returns null when fails. If one or more commands in the given list does not exist, the whole call fails.
  # You can use names:[''*''] to get properties for the whole set of commands.
  command_info(names:[String!] = ["*"]) : [CommandInfo!]

  # Error string or null when data are available or when mutation was ok. By default it returns only the
  # description field of the top level error stack. The error field must be placed at the end of the block.
  error(getStack:Boolean = false): [String]

}

# Mutation status
type MutationStatus {

  # Command output, null if command has failed or command has no output or if the mutation is not a command.
  # By default, DevState is returned as an integer, use resolveState:true to get state values as strings
  command_output(resolveState:Boolean = false): ScalarTypes

  # Error string or null when data are available or when mutation was ok. By default it returns only the
  # description field of the top level error stack. The error field must be placed at the end of the block.
  error(getStack:Boolean = false): [String]

}

# Device mutation object
type DeviceWrite {

  # Write an attribute. If the writing fails, the error field of the returned status is set.
  # For DevEnum or DevState, you can pass either an integer value or the value as string.
  # dimX and dimY are used only for image, the given array must have a length of dimX*dimY.
  write_attribute(name:String!,value:ScalarTypes!,dimX:Int=0,dimY:Int=0): MutationStatus

  # Execute a command. If the command fails, the error field of the returned status is set.
  # For DevVarLongStringArray, use argin:{long:[1,2,3],string:[''a'',''b'']} syntax.
  command_inout(name:String!,argin:ScalarTypes = null): MutationStatus

  # Set attribute config
  write_attribute_info(name:String!):SetAttributeInfo

  # Error string or null when data are available or when mutation was ok. By default it returns only the
  # description field of the top level error stack. The error field must be placed at the end of the block.
  error(getStack:Boolean = false): [String]

}

# Database mutation object
type DatabaseWrite {

  # Put DEVICE,CLASS or OBJECT properties. If the writing fails, the error field of the returned status is set.
  # Use value:[] to create an empty property and value:null to delete the property
  put_property(type:DbQueryType!,name:String!,propName:String!,value:[String]):MutationStatus!

  # Put DEVICE,CLASS or OBJECT attribute properties. If the writing fails, the error field of the returned status is set.
  # Use value:[] to create an empty property and value:null to delete the property
  put_attribute_property(type:DbQueryType!,name:String,attName:String,propName:String!,value:[String]):MutationStatus!

  # Add a server. If the writing fails, the error field of the returned status is set.
  # servName is the full process name (ie: exeName/instance), devNames and classNames must have the same length.
  add_server(serverName:String!,devNames:[String!]!,classNames:[String!]!):MutationStatus!

  # Delete a server. If the writing fails, the error field of the returned status is set.
  # servName is the full process name (ie: exeName/instance)
  delete_server(serverName:String!):MutationStatus!

  # Error string or null when data are available or when mutation was ok. By default it returns only the
  # description field of the top level error stack. The error field must be placed at the end of the block.
  error(getStack:Boolean = false): [String]

}

# Type containing command properties
type CommandInfo {

  # Command name
  name: String!
  # Command tag
  tag: Int!
  # Command input type
  in_type: String!
  # Command output type
  out_type: String!
  # Command input type description
  in_type_desc: String
  # Command output type description
  out_type_desc: String

}

# Mutation of attribute properties.
# In case of failure, the error field of the returned status is set.
type SetAttributeInfo {

  # Set Description
  description(value:String!): MutationStatus!
  # Set Label
  label(value: String!): MutationStatus!
  # Set Unit (Unit as string eg: ''Volt'',''mA'')
  unit(value: String!):  MutationStatus!
  # Standard unit (Conversion factor to MKSA unit)
  standard_unit(value: Float!):  MutationStatus!
  # Display unit (Conversion factor to unit)
  display_unit(value: Float!):  MutationStatus!
  # Attribute format, C like format (eg: ''%5.2f'')
  format(value: String!):  MutationStatus!
  # Minimum value (''NaN'' when not specified)
  min_value(value: ScalarTypes!):  MutationStatus!
  # Maximum value (''NaN'' when not specified)
  max_value(value: ScalarTypes!):  MutationStatus!
  # Minimum alarm threshold (''NaN'' when not specified)
  min_alarm(value: ScalarTypes!):  MutationStatus!
  # Maximum alarm threshold (''NaN'' when not specified)
  max_alarm(value: ScalarTypes!):  MutationStatus!
  # Time to wait before triggering an alarm if abs(value-writevalue)>delta_val (''NaN'' when not specified)
  delta_t(value: ScalarTypes!):  MutationStatus!
  # Threshold used in the delta alarm (''NaN'' when not specified)
  delta_val(value: ScalarTypes!):  MutationStatus!
  # ChangeEvent Relative change threshold
  change_rel_change(value: String!):  MutationStatus!
  # ChangeEvent Absolute change threshold
  change_abs_change(value: String!):  MutationStatus!
  # PeriodicEvent Relative change threshold
  periodic_period(value: String!):  MutationStatus!
  # ArchiveEvent Relative change threshold
  archive_rel_change(value: String!):  MutationStatus!
  # ArchiveEvent Absolute change threshold
  archive_abs_change(value: String!):  MutationStatus!
  # ArchiveEvent Period
  archive_period(value: String!):  MutationStatus!

}

# Type containing attribute properties.
type AttributeInfo {

  # Attribute name
  name: String!
  # Writable type (Read, Write,...)
  writable: String!
  # Data format (SCALAR, SPECTRUM, IMAGE)
  data_format: String!
  # Data type (DevBoolean,DevShort, ...)
  data_type: String!
  # Maximum X dimension
  max_dim_x: Int!
  # Maximum Y dimension
  max_dim_y: Int!
  # Description
  description: String!
  # Label
  label: String!
  # Unit (Unit as string eg: ''Volt'',''mA'')
  unit: String!
  # Standard unit (Conversion factor to MKSA unit, default 1)
  standard_unit: Float!
  # Display unit (Conversion factor to unit, default 1)
  display_unit: Float!
  # Attribute format, C like format (eg: ''%5.2f'')
  format: String!
  # Minimum value (''NaN'' when not specified)
  min_value: ScalarTypes!
  # Maximum value (''NaN'' when not specified)
  max_value: ScalarTypes!
  # Minimum alarm threshold (''NaN'' when not specified)
  min_alarm: ScalarTypes!
  # Maximum alarm threshold (''NaN'' when not specified)
  max_alarm: ScalarTypes!
  # Time to wait before triggering an alarm if abs(value-writevalue)>delta_val (''NaN'' when not specified)
  delta_t: ScalarTypes!
  # Threshold used in the delta alarm (''NaN'' when not specified)
  delta_val: ScalarTypes!
  # Enum labels
  enum_labels: [String!]!
  # Display level (OPERATOR, EXPERT)
  disp_level: String!
  # ChangeEvent Relative change threshold
  change_rel_change: String!
  # ChangeEvent Absolute change threshold
  change_abs_change: String!
  # PeriodicEvent Relative change threshold
  periodic_period: String!
  # ArchiveEvent Relative change threshold
  archive_rel_change: String!
  # ArchiveEvent Absolute change threshold
  archive_abs_change: String!
  # ArchiveEvent Period
  archive_period: String!

}

# Type containing result of attribute reading.
type DeviceAttribute {
  # Attribute name
  name: String!
  # Attribute quality factor (ATTR_ERROR when reading fails)
  quality: String!
  # Read value (null when reading fails). By default, DevEnum and DevState are returned as integer, use resolveEnum:true
  # to get value as a string
  value(resolveEnum:Boolean = false): ScalarTypes
  # Write value (setpoint) or null if not writable. By default, DevEnum and DevState are returned as integer,
  # use resolveEnum:true to get value as a string
  write_value(resolveEnum:Boolean = false): ScalarTypes
  # Time stamp (micro second resolution). formatAsDate allow to retrieve the timestamp as DD/MM/YYYY hh:mm:ss.us.
  timestamp(formatAsDate:Boolean = false): ScalarTypes!
  # X dimension
  dim_x: Int
  # Y dimension
  dim_y: Int
  # X write dimension (or 0 when not writable)
  w_dim_x: Int
  # Y write dimension (or 0 when not writable)
  w_dim_y: Int
  # Data type (DevBoolean,DevShort, ...)
  data_type: String
  # Data format (SCALAR, SPECTRUM, IMAGE)
  data_format: String
  # Encoded data format or null if the data is not DevEncoded
  encoded_format: String
  # Error string or null when data are available or when mutation was ok. By default it returns only the
  # description field of the top level error stack. The error field must be placed at the end of the block.
  error(getStack:Boolean = false): [String]
}

# Subscription strategy
enum EventSubscriptionMode {
  # Subscribe to change event
  CHANGE
  # Subscribe to periodic event
  PERIODIC
  # Subscribe to change event first if possible then default to periodic event.
  CHANGE_PERIODIC
  # Subscribe to configuration change event
  ATTR_CONF
}

# AttributeFrame sent via WebSocket
type AttributeFrame {

  # Full attribute name (lowercase)
  full_name: String!

  # Index in the subscription list
  index: Int!

  # Event type (change, periodic, attr_conf)
  # When the subscription_error is set, event contains the EventSubscriptionMode
  event:String!

  # Subscription error or null when subscription was ok
  subscription_error: String

  # Attribute value (null when subscription_error or attr_conf)
  value: DeviceAttribute

  # Attribute info (null when subscription_error or periodic or change)
  attribute_info: AttributeInfo

}


# Query type contains all queries.
type Query {

  # Schema introspection meta query (hidden do not remove)
  __schema: __Schema
  # Types introspection meta query (hidden do not remove)
  __type(name: String!): __Type
   # Dump the device factory names (hidden do not remove)
  __devnames: [String!]!

  # Main device query. Use tango://host:port/domain/family/member to get a device from a specific tango host.
  # If the device cannot be imported, the error field of the DeviceRead object is set.
  device(name:String!): DeviceRead!

  # Main database query, Use tangoHost=hostname:port or tangoHost=default to connect to the database specified by the
  # TANGO_HOST environment variable.
  # If the database cannot be accessed, the error field of the DatabaseRead object is set.
  database(tangoHost:String = "default"): DatabaseRead!

}

# Mutation type contains all mutations.
type Mutation {

  # Main device mutation. Use tango://host:port/domain/family/member to get a device from a specific tango host.
  # If the device cannot be imported, the error field of the DeviceWrite object is set.
  device(name:String!): DeviceWrite!

  # Main database mutation, Use tangoHost=hostname:port or tangoHost=default to connect to the database specified by the
  # TANGO_HOST environment variable.
  # If the database cannot be accessed, the error field of the DatabaseWrite object is set.
  database(tangoHost:String = "default"): DatabaseWrite!

}

# Subscription type contains all subscriptions.
# GraphQL WS protocol create one WebSocket per subscribe request.
# In order to minimize the number of connection, group subscriptions as much as possible.
type Subscription {

  # Main event subscription request.
  # The attNames array and the modes array must have the same length.
  # When one or more subscriptions fail, the subscription_error field of the returned DeviceAttributeFrame is set.
  subscribe(attNames:[String!]!,modes:[EventSubscriptionMode!]!):AttributeFrame

}

)"