//
// GQLType object
//

#ifndef TANGOGQL_GQLTYPE_H
#define TANGOGQL_GQLTYPE_H

#include "GQLEnums.h"
#include "GQLField.h"
#include "GQLInputArgs.h"

namespace TangoGQL_ns {

// Destroy the ofType chain up to the root type
// root type is managed by the schema

class GQLEngine;
class GQLField;

enum GQLTYPEKIND {
  SCALAR,       // Indicates this type is a scalar.
  OBJECT,       // Indicates this type is an object. fields and interfaces are valid fields.
  INTERFACE,    // Indicates this type is an interface. fields and possibleTypes are valid fields.
  UNION,        // Indicates this type is a union. possibleTypes is a valid field.
  ENUM,         // Indicates this type is an enum. enumValues is a valid field.
  INPUT_OBJECT, // Indicates this type is an input object. inputFields is a valid field.
  LIST,         // Indicates this type is a list. ofType is a valid field.
  NON_NULL,     // Indicates this type is a non-null. ofType is a valid field.
  INCOMPLETE    // Indicates this type has an incomplete definition
};

class GQLType: public GQLBase {

public:

  GQLType(const std::string& name,GQLTYPEKIND kind);
  ~GQLType();

  std::string name;
  GQLTYPEKIND kind;
  std::string description;
  std::vector<GQLField *> fields;
  std::vector<GQLField *> visibleFields;
  GQLEnums enumValues;
  GQLType *ofType; // ofType chain for LIST and NON_NULL
  int ofTypeLength;

  // Not supported
  //std::vector<std::string> interfaces;
  //std::vector<std::string> possiblesTypes;

  std::string to_string(bool isDefinition) const;
  GQLType *toRoot() const;
  bool isList() const;

  void parse(GQLSchema *schema,Parser* parser,std::string& def);

  GQLField *getField(const std::string& name) const;
  bool addField(GQLField *f);
  bool hasFields() const;
  void updateVisibleFields();

  std::string __typename() {
    return "__Type";
  }

  static std::string GetKind(GQLTYPEKIND k);
  static void DestroyOfTypeChain(GQLType *type);
  static GQLType * CloneOfTypeChain(GQLType *type);

private:

};

} // namespace TangoGQL_ns


#endif //TANGOGQL_GQLTYPE_H
