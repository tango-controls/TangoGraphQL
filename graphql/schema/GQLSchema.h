//
// GQL Schema
//

#ifndef TANGOGQL_GQLSCHEMA_H
#define TANGOGQL_GQLSCHEMA_H

#include "GQLType.h"

namespace TangoGQL_ns {

class GQLEngine;

class GQLSchema: public GQLBase {


public:

  GQLSchema();
  ~GQLSchema();

  std::string name;
  std::string description;
  std::vector<GQLType *> types;
  //std::vector<GQLDirective> directives;

  std::string to_string() const;
  void parse(Parser* parser);
  GQLType *parseTypeUsage(Parser* p);

  GQLType *getQueryType();
  GQLType *getMutationType();
  GQLType *getSubscriptionType();

  GQLType *getType(const std::string& typeName) const;
  GQLField *getQuery(const std::string& queryName) const;

  std::string __typename() {
    return "__Schema";
  }

private:
  GQLType *queryType;
  GQLType *mutationType;
  GQLType *subscriptionType;

};

} // namespace TangoGQL_ns

#endif //TANGOGQL_GQLSCHEMA_H
