# Tango GraphQL server

TangoGraphQL is a pure C++ Tango server that exports a GraphQL Tango bindings through a HTTP(S) web server.
TangoGraphQL has been designed to maximise performance of JSON data serialisation. TangoGraphQL uses fast floating point serialisation algorithm [Grisu2](https://www.cs.tufts.edu/~nr/cs257/archive/florian-loitsch/printf.pdf) from Florian Loitsch. A binary transfer mode is currently under development. TangoGraphQL server supports most of Tango types.
TangoGraphQL implements [GraphQL over WebSocket Protocol](https://github.com/enisdenjo/graphql-ws) which is now well supported by GraphiQL. The GraphQL team showed a very good reactivity to [improve subscription mechanism](https://github.com/graphql/graphiql/issues/1827).


[Link to GraphQL schema](https://gitlab.com/tango-controls/TangoGraphQL/-/blob/master/graphql/schema/GQLSchema.schema#L150)

The schema is also available from the HTTP(S) web server **http://host:port/schema.graphql** 

# Build

TangoGraphQL can be build as a standard Tango server using either make or cmake. It has no external specific dependencies and runs only on linux platform. [OpenSSL](https://www.openssl.org/) (-lssl and -lcrypo) is used for https.

# Configuration

![Config](https://gitlab.com/tango-controls/TangoGraphQL/-/raw/master/img/img4.jpg)

**Port**: HTTP server port (default is 8000)<br/>
**IdleTimeout**: Timeout before closing a HTTP connection (default is 60s)<br/>
**Authentication**: None or Basic<br/>
**Users**: List of allowed users, (List of userNanme:sha1Password)<br/>
**AllowMutation**: Allow mutation mode, ALWAYS,AUTHENTICATED,NEVER<br/>
**AllowMultiTangoHost**: Allow multi tango host, default is true<br/>
**exportGraphiQL**: True to enable GraphiQL (available via http(s)://host:port/graphiql/)<br/>
**httpsEnalbe**: Enalbe HTTPS server (need certificate)<br/>
**httpsCertificate**: Link to the cetificate file cert and its privKey file. (The server need read access to this 2 files)<br/>
**ResolveClientIP**: Resolve name in (connected) clients attribute
# GraphiQL

TangoGraphQL exports a GraphiQL interface to test the GraphQL bindings. GraphiQL is available from the url **http://host:port/graphiql/** 
where host is the host where TangoGraphQL server is running and port its http port (default 8000). The files needed to 
run the application are embedded in the executable so you just need to deploy the binary. 
Go [there](https://gitlab.com/tango-controls/TangoGraphQL/-/blob/master/graphiql/README.md) to see how to update GraphiQL (if needed).

![GraphiQL](https://gitlab.com/tango-controls/TangoGraphQL/-/raw/master/img/img1.jpg)

# Using javascript FetchAPI to access TangoDevice

```javascript
  query=
  `{
    d0: device(name:"simu/powersupply/1") {
      read_attributes(names:["state","current","voltage"]) {
        value(resolveEnum:true)
      }
    }
    d1: device(name:"simu/tg_test/1") {
      read_attributes(names:["double_scalar"]) {
        value
      }
    }
  }`
  let response: Response = await fetch("http://debian9acu:8000/graphql", {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    },
    body: JSON.stringify({ query: query })
  });


  let json;
  try {
    json = await response.json();
  } catch (error) {
    throw new Error("fetchGQL(): " + response.status + " " + response.statusText);
  }

  console.log("State=" + json.data.d0.read_attributes[0].value);
  console.log("Current=" + json.data.d0.read_attributes[1].value);
  console.log("Voltage=" + json.data.d0.read_attributes[2].value);
  console.log("double_scalar=" + json.data.d1.read_attributes[0].value);
```

```
State=ALARM
Current=-0.001
Voltage=-0.047
double_scalar=-238.99625241449579
```

Here is the same call through GraphiQL:

![GraphiQL](https://gitlab.com/tango-controls/TangoGraphQL/-/raw/master/img/img2.jpg)


## Handling NaN and Infinity

Unfortunatley JSON does not handle NaN or Infinity so TangoGraphQL returns them as string. Having unquoted NaN of Infinity in a json produces an exception during the parsing.

![GraphiQL](https://gitlab.com/tango-controls/TangoGraphQL/-/raw/master/img/img6.jpg)

It is possible to add a reviver function to the JSON parser that converts the 3 strings but this drasctically reduces performances when you want to parse a large dataset. It is much better to convert NaN and Infinity strings to numbers after the parsing using typeof to detect strings as below.

```javascript
  let json = JSON.parse(jsonStr);
  let value = json.data.device.read_attributes[0].value;
  console.log(value);
  for(i=0;i<value.length;i++) {
    if(typeof value[i]==="string") {
      if( value[i] === "NaN" )
        value[i] = NaN;
      else if (value[i] === "-Inf")
        value[i] = -Infinity;
      else if (value[i] === "Inf")
        value[i] = Infinity;
    }
  }
  console.log(value);
```
ouputs
```
[
  0,
  0.020720877837642555,
  'NaN',
  '-Inf',
  'Inf',
  0.10335491659708287,
  0.12388883462147326,
  0.14434812640471256
]
[
  0,
  0.020720877837642555,
  NaN,
  -Infinity,
  Infinity,
  0.10335491659708287,
  0.12388883462147326,
  0.14434812640471256
]
```

# Mutation (Write attributes, Set attribute configuration or Execute commands)

![Mutation](https://gitlab.com/tango-controls/TangoGraphQL/-/raw/master/img/img5.jpg)

# Subscribing to Tango Event

It is possible to subscribe to Tango event using [GraphQL WebSocket protocol](https://github.com/enisdenjo/graphql-ws). It is now fully supported in GraphiQL. You can check subscriptions and receive frames directly in the IDE. TangoGraphQL server implements this protocol.

![Subscribtion](https://gitlab.com/tango-controls/TangoGraphQL/-/raw/master/img/img3.jpg)

