// -------------------------------------------------------------------------------
// HTTP main thread
// -------------------------------------------------------------------------------
namespace TangoGQL_ns {
class HTTPServer;
}

#include "HTTPServer.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <libnet.h>

namespace TangoGQL_ns {

// ----------------------------------------------------------------------------------------
// Constructor
// ----------------------------------------------------------------------------------------
HTTPServer::HTTPServer(TangoGQL *tangoGql, omni_mutex &m, int p) :
        Tango::LogAdapter(tangoGql), mutex(m), ds(tangoGql), port(p) {

  exitThread = false;
  serverSock = -1;
  start_undetached();

}

// ----------------------------------------------------------------------------------------
// Main loop
// ----------------------------------------------------------------------------------------
void *HTTPServer::run_undetached(void *arg) {

  INFO_STREAM << "HTTPServer::run(): Start thread" << endl;

  // Create socket
  serverSock = socket(AF_INET,SOCK_STREAM,0);

  if(serverSock<0) {
    cerr << "Error: Invalid socket :" << string(strerror(errno)) << endl;
    return nullptr;
  }

  struct sockaddr_in soc_addr{};

  // Reuse Address
  int32_t yes = 1;
  if(setsockopt(serverSock,SOL_SOCKET,SO_REUSEADDR,(char *)&yes,sizeof(yes)) < 0) {
    ERROR_STREAM << "Warning: Couldn't Reuse Address: " << string(strerror(errno)) << endl;
  }

  // Bind
  memset(&soc_addr,0,sizeof(soc_addr));
  soc_addr.sin_family = AF_INET;
  soc_addr.sin_port = htons(port);
  soc_addr.sin_addr.s_addr = htonl(INADDR_ANY);

  if(bind(serverSock,(struct sockaddr*)&soc_addr,sizeof(soc_addr))) {
    cerr << "Error: Can not bind socket. Another server running?" << endl << string(strerror(errno)) << endl;
    return nullptr;
  }

  // Listen (listen queue size = 256)
  if(listen(serverSock,256)<0) {
    cerr << "Error: Can not listen to socket " << string(strerror(errno)) << endl;
    return nullptr;
  }
  cout << "HTTPServer: listen on port " << port << endl;

  // Accept loop
  while(!exitThread) {

    int clientSock;
    struct sockaddr_in client_add{};
    socklen_t len = sizeof(sockaddr_in);

    if((clientSock = accept(serverSock,(struct sockaddr*)&client_add,&len)) < 0) {

      if(!exitThread) {
        ERROR_STREAM << "Error: Invalid Socket returned by accept(): " << string(strerror(errno)) << endl;
      }

    } else {

      CLIENTINFO clientInfo;
      clientInfo.ip = string(inet_ntoa(client_add.sin_addr));
      clientInfo.port = ntohs(client_add.sin_port);
      clientInfo.name = "";
      clientInfo.socket = clientSock;
      int32_t yes = 1;
      if(setsockopt(clientSock,SOL_SOCKET,SO_REUSEADDR,(char *)&yes,sizeof(yes)) < 0) {
        ERROR_STREAM << "Warning: Couldn't Reuse Address: " << string(strerror(errno)) << endl;
      }
      if(ds->resolveClientIP) {
        // Resolve name
        socklen_t len;
        char hbuf[NI_MAXHOST];
        int err = getnameinfo((struct sockaddr *) &client_add, sizeof(sockaddr_in), hbuf, sizeof(hbuf),
                        NULL, 0, NI_NAMEREQD);
        if(err==0) {
          clientInfo.name = string(hbuf);
        } else {
          cout << "Error: " << gai_strerror(err) << endl;
        }
      }
      INFO_STREAM << "HTTPServer::accept("<<clientSock<<")" << endl;
      ds->connManager->start(clientInfo);

    }

  }

  INFO_STREAM << "HTTPServer::run(): End thread" << endl;
  return nullptr;

}

void HTTPServer::stop() {
  exitThread = true;
  shutdown(serverSock,SHUT_RDWR);
}


} // namespace TangoGQL_ns