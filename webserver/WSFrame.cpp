//
// WSFrame object
//


const char *OPCODE_STR[] = {"CONTINUATION","TEXT","BIN","RESV3","RESV4","RESV5","RESV6","RESV7",
                            "CLOSE","PING","PONG","RESVB","RESVC","RESVD","RESVE","RESVF"};

#include "WSFrame.h"
#include <iostream>

namespace TangoGQL_ns {

int WSFrame::addHeader(char b1,char b2) {

  int ret = 0;

  FIN =  ((unsigned)b1 & 0x80U) != 0;
  RSV1 = ((unsigned)b1 & 0x40U)!= 0;
  RSV2 = ((unsigned)b1 & 0x20U)!= 0;
  RSV3 = ((unsigned)b1 & 0x10U)!= 0;
  OPCODE = (unsigned)b1 & 0xFU;
  MASKED = ((unsigned)b2 & 0x80U)!= 0;
  PAYLOAD_L7 = (unsigned)b2 & 0x7FU;
  if( PAYLOAD_L7==126 ) {
    ret += 2;
  } else if( PAYLOAD_L7==127 ) {
    ret += 8;
  }
  if(MASKED) ret+=4;

  return ret;

}

int WSFrame::finalizeHeader(const char *bytes,int length) {

  int bPos = 0;

  if( PAYLOAD_L7==126 ) {
    uint8_t B16[2];
    B16[0] = bytes[bPos++];
    B16[1] = bytes[bPos++];
    payloadLength = (((uint64_t)B16[0]) << 8U) + (uint64_t)B16[1];
  } else if( PAYLOAD_L7==127 ) {
    uint8_t B64[8];
    for(unsigned char & b : B64)
      b = bytes[bPos++];
    payloadLength = (((uint64_t)B64[0]) << 56U) + (((uint64_t)B64[1]) << 48U) +
                    (((uint64_t)B64[2]) << 40U) + (((uint64_t)B64[3]) << 32U) +
                    (((uint64_t)B64[4]) << 24U) + (((uint64_t)B64[5]) << 16U) +
                    (((uint64_t)B64[6]) << 8U) + (uint64_t)B64[7];
  } else {
    payloadLength = (uint64_t)PAYLOAD_L7;
  }

  if( MASKED ) {
    for(char & m : mask)
      m = bytes[bPos++];
  }

  if( (int)bPos != length )
    std::cerr << "Warning::finalizeHeader() unexpected buffer length" << std::endl;

  return (int)payloadLength;

}

void WSFrame::addPayload(const char *bytes,int length) {

  payLoad.clear();

  if( (int)payloadLength != length )
    std::cerr << "Warning::addPayload() unexpected payloadLength" << std::endl;

  for(int i=0;i<length;i++) {
    char CM = MASKED?(char)((uint8_t )bytes[i] ^ (uint8_t )mask[i%4]):bytes[i];
    payLoad.push_back(CM);
  }

}

void WSFrame::TextFrame(const std::string& payLoad) {

  FIN = true;
  RSV1 = false;
  RSV2 = false;
  RSV3 = false;
  OPCODE = OPCODE_TEXT;
  MASKED = false;
  this->payLoad = payLoad;
  payloadLength = payLoad.length();

}

void  WSFrame::PingFrame() {

  FIN = true;
  RSV1 = false;
  RSV2 = false;
  RSV3 = false;
  OPCODE = OPCODE_PING;
  MASKED = false;
  payloadLength = 0;

}

void  WSFrame::CloseFrame(int statusCode,const std::string& desc) {

  FIN = true;
  RSV1 = false;
  RSV2 = false;
  RSV3 = false;
  OPCODE = OPCODE_CLOSE;
  MASKED = false;
  this->payLoad = "XX " + desc;
  payloadLength = payLoad.length();
  this->payLoad[0] = (char)((uint8_t)statusCode >> 8U);
  this->payLoad[1] = (char)((uint8_t)statusCode & 0xFFU);

}

void WSFrame::to_buffer(std::string &buff) {

  buff.reserve(16 + payloadLength);

  const uint8_t mFIN  = FIN?(1U << 7U):0U;
  const uint8_t mRSV1 = RSV1?(1U << 6U):0U;
  const uint8_t mRSV2 = RSV2?(1U << 5U):0U;
  const uint8_t mRSV3 = RSV3?(1U << 4U):0U;

  buff.push_back( mFIN | mRSV1 | mRSV2 | mRSV3 | OPCODE );

  if(payloadLength<126) {
    PAYLOAD_L7 = (uint8_t) payloadLength;
    buff.push_back( (uint8_t) ((uint8_t)((uint8_t )MASKED << 7U) | PAYLOAD_L7 ));
  } else if(payloadLength>=126 && payloadLength<65536) {
    PAYLOAD_L7 = 126;
    buff.push_back( (uint8_t) ((uint8_t)((uint8_t )MASKED << 7U) | PAYLOAD_L7 ));
    buff.push_back( (uint8_t) ((payloadLength & 0x000000000000FF00ULL)>>8U) );
    buff.push_back( (uint8_t) ((payloadLength & 0x00000000000000FFULL)>>0U) );
  } else {
    PAYLOAD_L7 = 127;
    buff.push_back( (uint8_t) ((uint8_t)((uint8_t )MASKED << 7U) | PAYLOAD_L7 ));
    buff.push_back( (uint8_t) ((payloadLength & 0xFF00000000000000ULL)>>56U) );
    buff.push_back( (uint8_t) ((payloadLength & 0x00FF000000000000ULL)>>48U) );
    buff.push_back( (uint8_t) ((payloadLength & 0x0000FF0000000000ULL)>>40U) );
    buff.push_back( (uint8_t) ((payloadLength & 0x000000FF00000000ULL)>>32U) );
    buff.push_back( (uint8_t) ((payloadLength & 0x00000000FF000000ULL)>>24U) );
    buff.push_back( (uint8_t) ((payloadLength & 0x0000000000FF0000ULL)>>16U) );
    buff.push_back( (uint8_t) ((payloadLength & 0x000000000000FF00ULL)>>8U) );
    buff.push_back( (uint8_t) ((payloadLength & 0x00000000000000FFULL)>>0U) );
  }

  if(MASKED) {
    buff.push_back(mask[0]);
    buff.push_back(mask[1]);
    buff.push_back(mask[2]);
    buff.push_back(mask[3]);
  }

  for(size_t i=0;i<payLoad.length();i++) {
    char CM = MASKED?(char)((uint8_t)payLoad[i] ^ (uint8_t)mask[i%4]):payLoad[i];
    buff.push_back(CM);
  }

}

int WSFrame::getCloseCode() {
  if(OPCODE==OPCODE_CLOSE && payloadLength >= 2) {
      uint16_t errCode = (((uint16_t) payLoad[0] & 0xFFU) << 8U) + ((uint16_t) payLoad[1] & 0xFFU);
      return (int)errCode;
  } else {
    return 0;
  }
}

std::string WSFrame::toString() {

  std::string ret;

  ret.append("WS Frame: ");
  ret.append(OPCODE_STR[OPCODE]);
  if( MASKED ) ret.append(" MASKED");

  ret.push_back(' ');
  if( payloadLength>0 ) {
    switch(OPCODE) {

#if 1
      case OPCODE_TEXT: {
        ret.append(payLoad);
      }break;
#endif

      case OPCODE_CLOSE: {
        int pos = 0;
        if (payloadLength >= 2) {
          uint16_t errCode = (((uint16_t) payLoad[0] & 0xFFU) << 8U) + ((uint16_t) payLoad[1] & 0xFFU);
          ret.append(std::to_string(errCode));
          ret.push_back(' ');
          pos += 2;
        }
        if ((int)payloadLength > pos) {
          ret.append(payLoad.substr(pos));
        }
      }break;

#if 0
      case OPCODE_BIN: {
        for (size_t i = 0; i < payloadLength; i++) {
          char buf[8];
          uint8_t c = (uint8_t) payLoad[i];
          sprintf(buf, "%02x ", c);
          ret.append(buf);
        }
      }break;
#endif

    }
  }

  return ret;

}


}
