// -------------------------------------------------------------------------------
// HTTP main thread
// -------------------------------------------------------------------------------

#ifndef TANGOGQL_HTTPSERVER_H
#define TANGOGQL_HTTPSERVER_H

#include <TangoGQL.h>
#include "webserver/Encoder.h"

namespace TangoGQL_ns {

class HTTPServer : public omni_thread, public Tango::LogAdapter {

public:

  // Constructor
  HTTPServer(TangoGQL *, omni_mutex &, int port);
  void *run_undetached(void *);
  void stop();

private:

  TangoGQL *ds;
  omni_mutex &mutex;
  int port;
  int serverSock;
  bool exitThread;

}; // class HTTPServer

} // namespace TangoGQL_ns

#endif //TANGOGQL_HTTPSERVER_H
