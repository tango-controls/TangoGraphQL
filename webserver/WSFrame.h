//
// WSFrame object
//

#ifndef TANGOGQL_WSFRAME_H
#define TANGOGQL_WSFRAME_H

#include <inttypes.h>
#include <string>

namespace TangoGQL_ns {

#define OPCODE_CONT 0x0
#define OPCODE_TEXT 0x1
#define OPCODE_BIN 0x2
#define OPCODE_CLOSE 0x8
#define OPCODE_PING 0x9
#define OPCODE_PONG 0xA

class WSFrame {

public:

  // Decoding
  int addHeader(char b1,char b2);
  int finalizeHeader(const char *bytes,int length);
  void addPayload(const char *bytes,int length);
  std::string toString();
  int getCloseCode();

  // Encoding
  void PingFrame();
  void TextFrame(const std::string& payLoad);
  void CloseFrame(int statusCode,const std::string& desc);
  void to_buffer(std::string &buff);


  std::string& getPayLoad() {
    return payLoad;
  }
  uint8_t getOpCode() {
    return OPCODE;
  }

private:

  uint64_t payloadLength;
  char mask[4];

  bool FIN;
  bool RSV1;
  bool RSV2;
  bool RSV3;
  bool MASKED ;
  uint8_t PAYLOAD_L7;
  uint8_t OPCODE;
  std::string payLoad;

};

} // namespace TangoGQL_ns

#endif //TANGOGQL_WSFRAME_H
