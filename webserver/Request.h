//
// HTTP request
//

#ifndef TANGOGQL_REQUEST_H
#define TANGOGQL_REQUEST_H

#include <TangoGQL.h>

namespace TangoGQL_ns {

typedef struct {
  std::string name;
  std::string value;
} HEADER;

class Request {

public:

  Request();

  /**
   * Get the specifed header of the request
   * @param name Header name
   * @param header Header value
   * @return true if the header exists, false otherwise
   */
  bool getHeader(const string& name,string &header);

  // Input request
  bool isComplete() const;
  string getURL() { return url; };
  void addChar(char c);
  void clear();
  string getType() { return type; };
  int getContentLength();
  std::string toString(bool withHeader);

  // Output request
  void addHeader(string& name,string& value);
  void createBadResponse(Tango::DevError &e);
  void createBadResponse(const string& message);
  void createUnauthorizedResponse(const string& message);
  void createForbiddenResponse();
  void createJSONResponse(const string& content);
  void createOptionResponse(const std::string& allowedRequests,bool auth);
  void createHelloResponse();
  void createArbResponse(void *buffer,int buffLength,const string& contentType);
  void createWebsocketHandshakeResponse(const std::string& key);
  void to_buffer(std::string& buff);

  static void getContentType(string& fileName,string &type);

private:

  std::string status;
  std::string content;
  std::string type;
  std::string url;
  std::string version;
  std::vector<HEADER> headers;

  std::string buffer;
  bool complete;
  int line;

}; // class{} Request

} // namespace TangoGQL_ns

#endif //TANGOGQL_REQUEST_H
