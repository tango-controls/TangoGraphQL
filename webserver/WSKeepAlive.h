//
// WSKeepAlive class
//

#ifndef TANGOGQL_WSKEEPALIVE_H
#define TANGOGQL_WSKEEPALIVE_H

#include <TangoGQL.h>
#include <DeviceFactory.h>
#include "../graphql/GQLEngine.h"

namespace TangoGQL_ns {

class WSKeepAlive : public omni_thread, public Tango::LogAdapter {

public:

  // Constructor
  /**
   * Starts a thread that PING to WebSocket every refreshPeriod millis
   * @param tangoGql Handle to ds
   * @param conn WebSocket Connection
   * @param refreshPeriod Refresh period in millis
   */
  WSKeepAlive(TangoGQL *tangoGql,Connection *conn, int refreshPeriod);
  void *run_undetached(void *);

private:

  Connection *conn;
  int refreshPeriod;

};

} // namespace TangoGQL_ns

#endif //TANGOGQL_WSKEEPALIVE_H
