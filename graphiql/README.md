# GraphiQL for TangoGraphQL

To build and embed the files into the TangoGraphQL server, you need to:
(You may need to edit package.json to change path to AppBuilder)

Make AppBuilder in the TangoGraphQL directory
```
make AppBuilder
```
Update GraphiQL version number in `package.json`

Go to the graphiql directory and install dependencies in nodes_modules
```
npm i
```
Check install
```
[ubuntu20acu.pons] > npm list graphiql graphql graphql-ws
tango-graphiql@1.0.0 /mnt/multipath-shares/segfs/tango/cppserver/protocols/TangoGraphQL/graphiql
├─┬ graphiql@3.2.2
│ ├─┬ @graphiql/react@0.22.1
│ │ ├─┬ codemirror-graphql@2.0.11
│ │ │ └── graphql@16.8.1 deduped
│ │ └── graphql@16.8.1 deduped
│ ├─┬ @graphiql/toolkit@0.9.1
│ │ ├── graphql-ws@5.16.0 deduped
│ │ └── graphql@16.8.1 deduped
│ ├─┬ graphql-language-service@5.2.0
│ │ └── graphql@16.8.1 deduped
│ └── graphql@16.8.1 deduped
├─┬ graphql-ws@5.16.0
│ └── graphql@16.8.1 deduped
└── graphql@16.8.1
```
Build GraphiQL
```
npm run build
```
Genrates the fs.h and fs.cpp files which contains the app
```
npm run build-src
```
Build the TangoGraphQL server
